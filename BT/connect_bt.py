import subprocess
from time import sleep
from rtp_control.start_send import start_send
from BT.bt_play_button import press_play

def connect_bt(meta):
    subprocess.run(["bluetoothctl", "connect", meta['mac_address']], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    # start_send(meta)
    sleep(3)
    press_play()
    return {"status": "done"}