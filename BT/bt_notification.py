from dasbus.loop import EventLoop
from requests import post
from rtp_control.start_send import start_send
from rtp_control.stop_send import stop_send
from vars import SERVICE_NAME, PLAYER_IFACE, bus, SERVER_ADDRESS, RASPBERRY_LOCATION, base_manager


def send_device_location_update(address, msg_type):
    r = post(f"{SERVER_ADDRESS}/devices", json={
            "type": msg_type,
            "meta": {
                "mac": address,
                "location": RASPBERRY_LOCATION
            }
        })
    return r


def bt_connected(*args, **kwargs):
    name, obj = args
    player_path = None
    for iface in obj.keys():
        if PLAYER_IFACE in iface:
            player_path = name
            device_path = obj[PLAYER_IFACE]['Device'].get_string()
    
    if not player_path:
        return
    
    device = bus.get_proxy(SERVICE_NAME, device_path, "org.bluez.Device1")
    
    print("Media device found")
    print(f"Device: {device.Address} - {device.Alias}")

    resp = send_device_location_update(device.Address, "device_connection")
    multicast = resp.json()
    print(multicast)
    if multicast != "Failed":
        meta = {'multicast_address': multicast}
        start_send(meta)


def bt_disconnected(*args, **kwargs):
    name, objs = args
    device_address = None
    for iface in objs:
        if PLAYER_IFACE in iface:
            device_address = name.split('/')[-2].strip('dev_').replace('_',':')
    
    if not device_address:
        return
    
    print("Media device disconnected")
    print(f"Device: {device_address}")

    resp = send_device_location_update(device_address, "device_disconnection")
    multicast = resp.json()
    print(multicast)
    if multicast != "Failed":
        stop_send({"multicast_address":multicast})


def start_bt_event_loop():
    loop = EventLoop()

    base_manager.InterfacesAdded.connect(bt_connected)
    base_manager.InterfacesRemoved.connect(bt_disconnected)
    print("Starting event loop")
    loop.run()
