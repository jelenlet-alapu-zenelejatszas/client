import subprocess
from rtp_control.stop_send import stop_send

def disconnect_bt(meta):
    stop_send(meta)
    subprocess.run(["bluetoothctl", "disconnect", meta['address']], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    return {"status": "done"}