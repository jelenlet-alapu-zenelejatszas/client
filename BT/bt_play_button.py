from vars import SERVICE_NAME, PLAYER_IFACE, bus, base_manager


def press_play():
    objects = base_manager.GetManagedObjects()
    player_path = None

    for path, iface in objects.items():
        if PLAYER_IFACE in iface.keys():
            player_path = path

    player = bus.get_proxy(SERVICE_NAME, player_path, "org.bluez.MediaPlayer1")
    player.Play()
    
    # if player.Status == "paused":
    #     print(player.Status)
    #     player.Play()

    # print("Currently playing: ")
    # print(str(player.Track['Artist']).strip('\'') + ": " + str(player.Track['Title']).strip('\''))