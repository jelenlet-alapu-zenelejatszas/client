from dasbus.connection import SystemMessageBus, InterfaceProxy

SERVICE_NAME = "org.bluez"
PLAYER_IFACE = SERVICE_NAME + '.MediaPlayer1'
bus = SystemMessageBus()
base_manager = InterfaceProxy(bus, SERVICE_NAME, '/', 'org.freedesktop.DBus.ObjectManager')

SERVER_ADDRESS = "http://192.168.100.43:5000"
RASPBERRY_LOCATION = 'lr1'  # location_id

BT_HANDOVER_THRESHOLD = 5