#! /bin/bash

sudo apt install bluez -y
sudo apt install pulseaudio-module-bluetooth -y
sudo apt install libgirepository1.0-dev -y
sudo apt install gcc -y
sudo apt install libcairo2-dev -y
sudo apt install pkg-config -y
sudo apt install python3-dev -y
sudo apt install gir1.2-gtk-4.0 -y