from flask import Flask, request
from flask_restful import Resource, Api
from rtp_control.start_recv import start_recv
from rtp_control.stop_recv import stop_recv
from rtp_control.start_send import start_send
from rtp_control.stop_send import stop_send
from rtp_control.measure_rssi import measure_rssi
from BT.connect_bt import connect_bt
from BT.disconnect_bt import disconnect_bt

app = Flask(__name__)
api = Api(app)


def type_error(meta):
    return {'error': "Unknown type"}

dispatcher = {
    "stop_send": stop_send,
    "start_send": start_send,
    "start_recv": start_recv,
    "stop_recv": stop_recv,
    "measure_rssi": measure_rssi,
    "disconnect_bt": disconnect_bt,
    "connect_bt": connect_bt
}

class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}
    
    def post(self):
        args = request.get_json()
        """
        Expected JSON format
        {
            "type": [start_send, start_recv, stop_send, stop_recv, measure_rssi],
            "meta": {
                "multicast_address": multicast,
                "mac_address": mac,
                "device_location": location_id,
                "to_location": location_id,
                "from_location": location_id
            }
        }
        """
        resp = dispatcher.get(args['type'], type_error)(args['meta'])
        return resp


api.add_resource(HelloWorld, '/')

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5001)