import subprocess
from vars import RASPBERRY_LOCATION


def start_recv(meta):
    if meta['to_location'] == RASPBERRY_LOCATION and meta['device_location'] == RASPBERRY_LOCATION:
        mac_address = meta['mac_address'].replace(':','_')
        subprocess.run(["pactl", "load-module", "module-loopback", f"source=bluez_source.{mac_address}.a2dp_source"," source_dont_move=true", \
                        "sink_input_properties=media.role=music"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    else:
        subprocess.run(["pactl", "load-module", "module-rtp-recv", f"sap_address={meta['multicast_address']}"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    return {"status": "done"}