import subprocess
from time import sleep
from vars import BT_HANDOVER_THRESHOLD


def get_rssi(address):
    resp = subprocess.run(["hcitool", "rssi", address], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    rssi = int(resp.stdout.strip('\n').split(':')[1])   # 'RSSI return value: -8\n'
    return rssi

def handover_decision(address):
    rssi_1 = get_rssi(address)
    print(rssi_1)
    
    sleep(2)

    rssi_2 = get_rssi(address)
    print(rssi_2)

    if abs(rssi_2 - rssi_1) > BT_HANDOVER_THRESHOLD:
        return True
    return False


def measure_rssi(meta):
    handover = handover_decision(meta['address'])
    print(handover)
    return {"IsHandover": handover}