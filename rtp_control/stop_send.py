import subprocess

def stop_send(meta):
    subprocess.run(["pactl", "unload-module", "module-rtp-send"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    return {"status": "done"}