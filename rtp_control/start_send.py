import subprocess

def start_send(meta):
    subprocess.run(["pactl", "load-module", "module-rtp-send", f"destination={meta['multicast_address']}"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    return {"status": "done"}