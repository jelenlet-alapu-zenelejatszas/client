import subprocess
from vars import RASPBERRY_LOCATION


def stop_recv(meta):
    if meta['from_location'] == RASPBERRY_LOCATION and meta['device_location'] == RASPBERRY_LOCATION:
        subprocess.run(["pactl", "unload-module", "module-loopback"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    else:
        subprocess.run(["pactl", "unload-module", "module-rtp-recv"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    return {"status": "done"}